<?php
/**
 * Created by PhpStorm.
 * User: Cat Studio
 * User Id: 007
 * Date: 24.07.2018
 */
namespace DI\Test;
class ExampleClass
{
    public $view;
    public $params = 'anything';
    public $dic = 'hello';
    protected $private = 'one';
    public function __construct(ExampleViewClass $testView)
    {
        $this->view = $testView;
    }
}