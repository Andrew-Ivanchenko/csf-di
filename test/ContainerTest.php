<?php
/**
 * Cat Studio Development.
 * kollins83@gmail.com
 */

namespace CSF\DI;

use DI\Test\ExampleClass;
use DI\Test\ExamplePostClass;
use DI\Test\ExampleViewClass;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    /**
     * @var Container
     */
    public $container;

    public $testingClassName = [
        ExampleClass::class,
        ExampleViewClass::class,
        ExamplePostClass::class
    ];

    /**
     *
     * @return Container
     */
    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        return $this->container = new Container();
    }

    /**
     * Тест создания новых объектов методом get
     * @throws Exception\InstantiableException
     * @throws Exception\ServiceException
     */
    public function testToCreateObjectUsingGet() :void
    {
        foreach ($this->testingClassName as $class) {
            $object =  $this->container->get($class);
            $this->assertInternalType('object', $object);
        }
    }

    /**
     * Тестирует попадает ли в массив service идентификатор и имя зависимости
     * Если имя класса не задкларировано, объекта не существует или в массиве нет ключа 'class' - будет вызвано исключение
     * @throws Exception\ServiceException
     */
    public function testServiceHasKey() :void
    {
        //Имя класса
        $this->container->set(ExampleClass::class);
        $this->assertArrayHasKey(ExampleClass::class, $this->container->getServices());
        //Объект
        $this->container->set(new ExamplePostClass());
        $this->assertArrayHasKey(ExamplePostClass::class, $this->container->getServices());
        //Идентификатор и имя класса
        $this->container->set('test', ExampleClass::class);
        $this->assertArrayHasKey('test', $this->container->getServices());
        //Идентификатор и объект
        $this->container->set('testNew', new ExamplePostClass());
        $this->assertArrayHasKey('testNew', $this->container->getServices());
        //Идентификатор и массив с ключом 'class'
        $this->container->set('testClass', ['class' => ExampleClass::class]);
        $this->assertArrayHasKey('testClass', $this->container->getServices());
    }

    /**
     * Тестирует попадают ли параметры в массив params
     * @throws Exception\ServiceException
     */
    public function testParamsHasKey() :void
    {
        $this->container->set(ExampleClass::class);
        $this->assertAttributeNotEmpty('params', $this->container);
    }

    /**
     * Тестирует создание зависимости из имени класса
     * и попалает ли зависимость в массив dependency
     * @throws Exception\InstantiableException
     * @throws Exception\ServiceException
     */
    public function testGetDependencyClassNameWithoutParams() :void
    {
        $this->container->set(ExampleClass::class);
        $object = $this->container->get(ExampleClass::class);
        $this->assertInternalType('object', $object);
        $this->assertArrayHasKey(ExampleClass::class, $this->container->getDependency());
    }

    /**
     * Тестирует создание зависимости из объекта
     * и попалает ли зависимость в массив dependency
     * @throws Exception\InstantiableException
     * @throws Exception\ServiceException
     */
    public function testGetDependencyObjectWithoutParams() :void
    {
        $this->container->set(new ExamplePostClass());
        $object = $this->container->get(ExamplePostClass::class);
        $this->assertInternalType('object', $object);
        $this->assertArrayHasKey(ExamplePostClass::class, $this->container->getDependency());
    }

    /**
     * Тестирует создание зависимости из имени класса через идентификатор
     * и попалает ли зависимость в массив dependency
     * @throws Exception\InstantiableException
     * @throws Exception\ServiceException
     */
    public function testGetDependencyIdClassName() :void
    {
        $this->container->set('test', ExampleClass::class);
        $object = $this->container->get('test');
        $this->assertInternalType('object', $object);
        $this->assertArrayHasKey(ExampleClass::class, $this->container->getDependency());
    }

    /**
     * Тестирует создание зависимости из объекта через идентификатор
     * и попалает ли зависимость в массив dependency
     * @throws Exception\InstantiableException
     * @throws Exception\ServiceException
     */
    public function testGetDependencyIdObject() :void
    {
        $this->container->set('test', new ExamplePostClass());
        $object = $this->container->get('test');
        $this->assertInternalType('object', $object);
        $this->assertArrayHasKey(ExamplePostClass::class, $this->container->getDependency());
    }

    /**
     * Тестирует создание зависимости из массива с ключом 'class' и имени класса через идентификатор
     * и попалает ли зависимость в массив dependency
     * @throws Exception\InstantiableException
     * @throws Exception\ServiceException
     */
    public function testGetDependencyIdArray() :void
    {
        $this->container->set('test', ['class' => ExamplePostClass::class]);
        $object = $this->container->get('test');
        $this->assertInternalType('object', $object);
        $this->assertArrayHasKey(ExamplePostClass::class, $this->container->getDependency());
    }

    /**
     * Тестирует создание зависимости из массива с ключом 'class' и объектом через идентификатор
     * и попалает ли зависимость в массив dependency
     * @throws Exception\InstantiableException
     * @throws Exception\ServiceException
     */
    public function testGetDependencyIdArrayObj() :void
    {
        $this->container->set('test', ['class' => new ExamplePostClass()]);
        $object = $this->container->get('test');
        $this->assertInternalType('object', $object);
        $this->assertArrayHasKey(ExamplePostClass::class, $this->container->getDependency());
    }

    /**
     * Тест магических методов
     */
    public function testMagicMethods() :void
    {
        $this->container->test = ExamplePostClass::class;
        $this->assertInternalType('object', $this->container->test);
    }

    /**
     * Проверка анонимной функции из магических методов
     * @throws Exception\InstantiableException
     * @throws Exception\ServiceException
     */
    public function testLambdaFunction()
    {
        $this->container->testLambda = function (){return new ExamplePostClass();};
        $object = $this->container->get('testLambda');
        $this->assertInternalType('object', $object);
        //OR
        $this->assertInternalType('object', $this->container->testLambda);
    }

    /**
     * Проверка анонимной функции методом get
     * @throws Exception\InstantiableException
     * @throws Exception\ServiceException
     */
    public function testLambdaFunctionFromGet()
    {
        $object = $this->container->get(function () {return new ExamplePostClass();});
        $this->assertInternalType('object', $object);
    }
}
