#Dependency injection (Зависимости)
[![License](https://poser.pugx.org/csf/csf-di/license)](https://packagist.org/packages/csf/csf-di)
###Список классов

* DI\Di (Abstract)
    * DI\Container
* DI\Exception\InstantiableException
* DI\Exception\ServiceException

###Описание

Объект Di - знает как создавать, настраивать объекты и все их зависимые объекты
Di поддерживает внедрение зависимостей через конструктор класса, а так же может переопределять все свойства класса.
Di может создавать новые экземпляры объектов без необходимости использования оператора 'new', и автоматически определит и
 поставит все необходимые зависимости.
 
***
 
###Установка 

Composer 
```text
composer require csf/csf-di:1.0.0
```

```json
{
"require": {
        "csf/csf-di": "1.0.0"
    }
}    
```
 
###Примеры
####Классы для тестирования
####test\TestClass

```php
<?php
namespace test;
class TestClass
{
    public $view;
    public $params = 'anything';
    public function __construct(\test\TestView $testView) 
    {
        $this->view = $testView;
    }
}
```
####test\TestView

```php
<?php
namespace test;
class TestView 
{
    public $post;
    public function __construct(\test\TestPost $testPost) 
    {
        $this->post = $testPost;
    }
}
```
####test\TestPost

```php
<?php
namespace test;
class TestPost
{
    public function render($id)
    {
        //делаем что-нибудь
    }  
}
```
####DI\Container

```php
<?php
$container = new \DI\Container();
```
 
####Способы внедрения зависимостей
 
Создаст объект `new TestClass(new TestView(new TestPost()))`

```php
<?php
$testClass = $container->get(\test\TestClass::class); //объект \test\TestClass
//или
$testPost = $container->get(function (){return new \test\TestPost();}); //объект \test\TestPost()
```
 
Объявить зависимости в методе `set`
 
```php
<?php
$container->set(\test\TestClass::class);
$container->get(\test\TestClass::class); //объект \test\TestClass
/* Для обращения через идентификатор */
$container->set('testClass', \test\TestClass::class);
$container->get('testClass'); //объект \test\TestClass
/* Массивом через ключ 'class' */
$container->set('testClass', ['class' => \test\TestClass::class]);
$container->get('testClass'); //объект \test\TestClass
```

Объявить зависимости с переопределением свойств

```php
<?php     
$container->set(\test\TestClass::class, ['params' => null]);
$container->set('testClass', [
             'class' => \test\TestClass::class,
             // задать новое значение для свойства
             'params' => null
         ]
     );
/* Переопределить свойства методом setProperty() */
$container->set('testClass', \test\TestClass::class);
$container->setProperty('testClass', ['params' => null]);
$container->get('testClass');
```

Также можно переопределить закрытые и защищенные свойства установив значение свойства propertyVisible - `true` 

```php
<?php
$propertyVisible = true;
```

Можно использовать магические методы
 
```php
<?php
$container->testClass = \test\TestClass::class;
$container->testView = new \test\TestView(new \test\TestPost());
//или
$container->testPost = function (){ return new \test\TestPost(); };
```

Если класс внедряет зависимость через интерфейс, то в параметрах нужно указать ключ ['di'] и значение - класс который его реализует
Предположим что классы `TestView` и `TestPost` реализуют один и тот же интерфейс `TestInterface`, а класс `TestClass` в конструкторе 
его принимает. 
```php
<?php
...
public function __construct(TestInterface $testInterface){...}
```

Тогда реализация DI, будет

```php
<?php
$container->set(\test\TestClass::class, ['di' => TestView::class....]);
//или для обращения через идентификатор
$container->set('test', ['class' => \test\TestClass::class, 'di' => TestView::class....]);
```
