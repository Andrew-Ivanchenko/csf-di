<?php

namespace CSF\DI;

use CSF\DI\Exception\ServiceException;

/**
 * Class Container
 * @package DI
 */
class Container extends Di
{
    /**
     * Magic method __set is run when writing data to inaccessible properties.
     * Магический метод __set
     * Будет выполнен при записи данных в недоступные свойства.
     * @example
     *      $container = new Container();
     *      $container->new_property = \namespace\ClassName::class;
     *      $container->new_property = new ClassName();
     *      $container->new_property = function(){ return new ClassName; }
     *
     *      $container->new_property - будет содержать объект ClassName (will contain a ClassName object)
     *
     * @link http://php.net/manual/ru/language.oop5.overloading.php#object.set
     * @param $service
     * @param $params
     * @throws ServiceException
     */
    public function __set($service, $params)
    {
        if (\is_object($params) && $object = $this->closure($params)) {
            $params = $object;
        }
        $this->set($service, $params);
    }

    /**
     * Magic method __get is utilized for reading data from inaccessible properties.
     * Магический метод __get
     * Будет выполнен при чтении данных из недоступных свойств.
     * @link http://php.net/manual/ru/language.oop5.overloading.php#object.get
     * @param $name
     * @return object
     * @throws ServiceException
     * @see __set
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * Magic method __isset
     * Магический метод __isset
     * @link http://php.net/manual/ru/language.oop5.overloading.php#object.isset
     * @param $name
     * @return bool
     * @throws ServiceException
     * @throws Exception\ServiceException
     */
    public function __isset($name)
    {
        return $this->has($name);
    }
}