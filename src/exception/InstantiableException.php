<?php
/**
 * Cat Studio Development.
 * kollins83@gmail.com
 */


namespace CSF\DI\Exception;


use Throwable;

class InstantiableException extends \Exception
{
    public function __construct($class, string $message = null, int $code = 0, Throwable $previous = null)
    {
        if ($message === null){
            $message = "Can not instantiate $class";
        }
        parent::__construct($message, $code, $previous);
    }
}