<?php
/**
 * Cat Studio Development.
 * kollins83@gmail.com
 */


namespace CSF\DI\Exception;


use Throwable;

class ServiceException extends \Exception
{
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}