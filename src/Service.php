<?php

namespace CSF\DI;

use CSF\DI\Exception\ServiceException;

/**
 * Class Service
 * @package CSF\DI
 *
 * @author Andrew Ivanchenko <ivanchenko2007@ukr.net>
 */
abstract class Service
{
    /**
     * @var \ReflectionClass
     */
    private $reflection;

    /**
     * Return object \ReflectionClass
     * Возвращает объект \ReflectionClass
     * @return \ReflectionClass
     */
    protected function Reflection() :\ReflectionClass
    {
        return $this->reflection;
    }

    /**
     * Creates a ReflectionClass object
     * Создает объект ReflectionClass
     * @param $object
     * @return \ReflectionClass
     */
    protected function ReflectionClass($object) :\ReflectionClass
    {
        try {
            return new \ReflectionClass($object);
        } catch (\ReflectionException $exception) {
            throw new $exception($exception->getMessage());
        }
    }

    /**
     * @param $class
     * @param \ReflectionClass $reflectionClass
     * @return \ReflectionClass
     * @throws ServiceException
     */
    protected function setReflectionClass($class, \ReflectionClass $reflectionClass) :\ReflectionClass
    {
        $class = $this->serviceControl($class);
        return $this->reflection[$class] = $reflectionClass;
    }

    /**
     * Checks that the $class parameter is a string or an object.
     * Проверяет что параметр $class является строкой или объектом
     * @param $class
     * @return string имя класса или идентификатор (class name or id)
     * @throws ServiceException
     */
    protected function serviceControl($class) :string
    {
        if (\is_object($class)) {
            return \get_class($class);
        }
        if (\is_string($class)) {
            return $class;
        }
        throw new ServiceException('Service name, must be a string or an object. (' . \gettype($class) . ') received');
    }

    /**
     * Build parameters for dependencies
     * Removes (if present) the 'class' key and returns an empty array if the $params parameter is not array.
     *
     * Сборка параметров для зависимостей
     * Удаляет (если присутствует) ключ 'class' и возвращает пустой массив если параметр $params не является таковым.
     * @param $params
     * @return array
     */
    protected function paramsAssembly(&$params) :array
    {
        if (!\is_array($params)) {
            $params = [];
        }else {
            if (isset($params['class'])) {
                unset($params['class']);
            }
            if (isset($params['di'])) {
                $di = $params['di'];
                if (\is_object($di) || (\is_string($di) && class_exists($di))) {
                    $params['di'] = $this->ReflectionClass($di);
                }else {
                    unset($params['di']);
                }
            }
        }
        return $params;
    }

    /**
     * Returns the class name for the dependency if the conditions
     * Возвращает имена классов для зависимостей если соблюдены условия
     * @param $service
     * @param mixed $params
     * @return string
     * @throws ServiceException
     * @see set()
     */
    protected function serviceNameBuilder($service, $params = '') :string
    {
        if ($params === null){
            return $service;
        }
        if (\is_array($params)) {
            if (!isset($params['class'])) {
                if (class_exists($service)) {
                    $params['class'] = $service;
                }else{
                    throw new ServiceException('A class definition requires a "class" member.');
                }
            }
            return $this->serviceControl($params['class']);
        }
        if (\is_object($params)){
            return $this->serviceControl($params);
        }
        if (\is_string($params)) {
            if (class_exists($params)) {
                return $params;
            }
            throw new ServiceException("The string \"$params\" is not a class name");
        }
        return $params['class'];
    }

}