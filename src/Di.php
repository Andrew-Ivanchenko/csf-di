<?php

namespace CSF\DI;

use CSF\DI\Exception\InstantiableException;
use CSF\DI\Exception\ServiceException;

/**
 * Class Di implements a dependency injection pattern (dependency injection)
 * Class Di реализует паттерн внедрения зависимостей (dependency injection)
 * @link https://en.wikipedia.org/wiki/Dependency_injection
 * @link https://ru.wikipedia.org/wiki/%D0%92%D0%BD%D0%B5%D0%B4%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B7%D0%B0%D0%B2%D0%B8%D1%81%D0%B8%D0%BC%D0%BE%D1%81%D1%82%D0%B8
 *
 * Di object - knows how to create, configure objects and all their dependent objects
 * Di supports dependency injection through a class constructor, and can also override all properties of the class.
 * Di can create new instances of objects without the need to use the operator 'new', and automatically detect and
 * put all the necessary dependencies.
 *
 * Объект Di - знает как создавать, настраивать объекты и все их зависимые объекты
 * Di поддерживает внедрение зависимостей через конструктор класса, а так же может переопределять все свойства класса.
 * Di может создавать новые экземпляры объектов без необходимости использования оператора 'new', и автоматически определит и
 * поставит все необходимые зависимости.
 *
 * @package DI
 */
abstract class Di extends Service
{
    /**
     * Array of generated dependencies by the set() method
     * Массив сформированных зависимостей методом set()
     * @var array
     */
    private $service = [];

    /**
     * Parameters (properties) of the dependent class
     * Параметры (свойства) зависимого класса
     * @var array
     */
    private $params = [];

    /**
     * Переопределение закрытых и защищенных свойств
     * @var bool
     */
    private $propertyVisible = false;

    /**
     * An array of formed dependencies by the get() method.
     * Массив сформированных зависимостей методом get()
     * @var array
     */
    private $dependency = [];

    /**
     * Sets new dependencies
     * Overrides properties of specified dependencies.
     *
     * Устанавливает новые зависимости
     * Переопределяет свойства указанных зависимостей
     * Правила (Rules)
     *
     * The service name must be a string (identifier, class name) or an object
     * Имя сервиса должно быть строкой (идентификатор, имя класса) или объектом
     *      $service - param string|object $service (идентификатор (id) | имя класса (class name) | объект (object))
     *
     * If the service name is a class name or an object, in the parameters you can override the
     * properties of the dependent class with an array
     * Если имя сервиса является именем класса или объектом, в параметрах можно массивом
     * переопределить свойства зависимого класса
     *      $params - param array $params (массив параметров (parameters array) ) ['property' => 'new value'.....]
     *
     * If the service name is an identifier, the parameter should take:
     * Если название сервиса является идентификатором, параметр должен принимать:
     *      $params - param string $params (имя класса (class name)) \namespace\ClassName::class || \namespace\ClassName
     *      $params - param object $params (объект (object)) new ClassName
     *      $params - param array $params (массив с ключом 'class' (array with the key 'class')) ['class' => new ClassName] || ['class' => \namespace\ClassName]
     *
     * @example
     * $container = new \DI\Container();
     *  Без параметров (without parameters)
     *      $container->set(\namespace\ClassName::class)
     *      $container->set(new ClassName())
     *  С параметрами (With parameters)
     *      $container->set('id', \namespace\ClassName::class)
     *      $container->set('id', new ClassName())
     *      $container->set('id', ['class' => \namespace\ClassName::class | new ClassName()])
     *  С переопределением свойств зависимого класса (Overriding properties of the dependent class)
     *      $container->set(\namespace\ClassName::class, ['property' => 'new value'.....])
     *      $container->set(new ClassName, ['property' => 'new value'.....])
     *      $container->set(id, ['class' => \namespace\ClassName::class || new ClassName, 'property' => 'new value'.....])
     *
     *  You can also override private and protected properties. $propertyVisible = true
     *  Также можно переопределить закрытые и защищенные свойства $propertyVisible = true
     *
     * IMPORTANT! If a class injects a dependency through an interface, then in the parameters you need to specify the
     * key ['di'] and the value - the class that implements it
     * ВАЖНО! Если класс внедряет зависимость через интерфейс, то в параметрах нужно указать
     * ключ ['di'] и значение - класс который его реализует
     *
     * @example
     * class Test implements TestInterface{}
     * class TestAnother implements TestInterface{}
     * class Implementation {
     *      public function __construct(TestInterface $testInterface){....}
     * }
     * $container = new \DI\Container();
     *      $container->set(Implementation::class, ['di' => Test::class]);
     *      $container->set('test', ['class' => Implementation::class, 'di' => Test::class])
     *
     *
     *
     * @param string|object $service
     * @param null|array|string $params
     * @return $this
     * @throws ServiceException
     * @see setProperties
     */
    public function set($service, $params = null) :self
    {
        $service = $this->serviceControl($service);
        if (!isset($this->service[$service]) || empty($this->service[$service])) {
            $this->service[$service] = $this->serviceNameBuilder($service, $params);
        }
        $this->params[$service] = $this->paramsAssembly($params);
        return $this;
    }

    /**
     * Returns an object with its default dependencies.
     * Adds a new dependency object to the dependency property
     *
     * Возвращает объект со свеми установленными по умолчанию зависимостями
     * Добавляет в свойство dependency новый объект зависимостей
     * Правила (Rules):
     *  Если зависимости объявлены в методе set() (If dependencies are declared in the set() method)
     *      get(id || class name || object);
     *
     *  В других случаях будет создан новый объект со всеми его зависимостями без переопределения свойств
     *  (In other cases, a new object will be created with all its dependencies without overriding properties.)
     *      get(\namespace\ClassName::class)
     *      get(new ClassName)
     *
     * @example
     *      $container = new Container();
     *
     *      $container->set('test', \namespace\ClassName::class);
     *      Если нужно переопределить свойства (If you need to override properties)
     *      $container->params('test', ['property' => 'new value'.....]);
     *      $container->get('test');
     *
     *      $container->set(\namespace\ClassName::class);
     *      $container->get(\namespace\ClassName::class);
     *
     *      $container->set(new ClassName);
     *      $container->get(\namespace\ClassName::class);
     *
     * @param string|object $service
     * @return object
     * @throws ServiceException
     */
    public function get($service) :object
    {
        $idService = $this->serviceControl($service);
        if (isset($this->dependency[$idService])) {
            return $this->dependency[$idService];
        }
        if ($this->has($idService)) {
            return $this->resolve($this->service[$idService], $this->params[$idService]);
        }
        $object = $this->closure($service);
        if (!\is_object($object)) {
            return $this->resolve($this->serviceNameBuilder($idService));
        }
        return $object;
    }

    /**
     * Defines the need to redefine closed and protected properties of an object.
     * Определяет необходимость переопределять закрытые и защищенные свойства объекта
     * @param bool $visible
     * @return bool
     */
    public function propertyVisible(bool $visible) :bool
    {
        return $this->propertyVisible = $visible;
    }

    /**
     * Checks if dependencies were defined before they were declared in the get() method
     * Проверяет были ли определены зависимости до их объявленя в методе get()
     * @param $id
     * @return bool
     * @throws ServiceException
     */
    public function has($id) :bool
    {
        $id = $this->serviceControl($id);
        return isset($this->service[$id]);
    }

    /**
     * Returns an array of defined dependencies via the set() method
     * Возвращает массив заданых зависимостей через метод set()
     * @return array
     * @see set()
     */
    public function getServices() :array
    {
        return $this->service;
    }

    /**
     * Returns an array of declared dependencies
     * Возвращает массив объявленых зависимостей
     * @return array
     */
    public function getDependency() :array
    {
        return $this->dependency;
    }

    /**
     * Creating dependencies from anonymous functions
     * Создание зависимостей из анонимных функций
     * @param callable $service
     * @return object|bool
     * @throws ServiceException
     */
    protected function closure($service)
    {
        if ($service instanceof \Closure) {
            $closure = $service($this); //also that - call_user_func($service, $this);
            if (!\is_object($closure)) {
                throw new ServiceException('An anonymous function must return an object, given a (' . \gettype($closure) . ')');
            }
            $idClosure = strtolower(\get_class($service)) . ':' . \get_class($closure);
            return $this->setDependency($idClosure,$this->resolve($closure));
        }
        return false;
    }

    /**
     * Add dependencies after they are declared in the get() method
     * Добавление зависимостей, после их объявления в методе get()
     * @param $id
     * @param $object
     * @return mixed
     * @throws ServiceException
     */
    private function setDependency($id, $object)
    {
        $id = $this->serviceControl($id);
        return $this->dependency[$id] = $object;
    }

    /**
     * Overrides private and protected properties
     * Переопределяет закрытые и защищенные свойства
     * @param $objects
     * @param $name
     * @return mixed
     */
    private function &getCloseProperty($objects, $name)
    {
        $reader = \Closure::bind(function &($object) use ($name) {
            return $object->$name;
        }, null, $objects);
        return $reader($objects);
    }

    /**
     * Creates a new object with passing parameters to the constructor.
     * Overrides properties if specified in $ params
     *
     * Создает новый объект с передачей параметров в конструктор
     * Переопределяет свойства, если они указаны в $params
     * @param $service
     * @param array $params
     * @return object
     * @throws ServiceException
     * @see set()
     * @see setProperties
     */
    private function resolve($service, array $params = []) :object
    {
        /* @var $reflection \ReflectionClass */
        /* @var $dependencies array */
        [$reflection, $dependencies] = $this->dependencies($service, $params);
        foreach ($dependencies as $key => $dependency) {
            if (class_exists($dependency)){
                $dependencies[$key] = $this->resolve($dependency, $params);
            }
        }
        try {
            if (!$reflection->isInstantiable()) {
                throw new InstantiableException($reflection->name);
            }
        }catch (InstantiableException $exception){
            throw new $exception($exception->getMessage());
        }
        $object = $reflection->newInstanceArgs($dependencies);
        foreach ($params as $name => $param){
            if (property_exists($object, $name)) {
                if ($this->propertyVisible === true) {
                    $properties = $reflection->getProperty($name);
                    if ($properties->isPrivate() || $properties->isProtected()) {
                        $property = &$this->getCloseProperty($object, $name);
                        $property = $param;
                        continue;
                    }
                }
                $object->$name = $param;
            }else {
                throw new ServiceException('Setting unknown property: (' . \get_class($object) . '::' . $name . ')');
            }
        }
        $this->setReflectionClass($service, $reflection);
        $this->setDependency($service, $object);
        return $object;
    }

    /**
     * Checks for the existence of a constructor and passes default values.
     * Returns an array with elements [\ ReflectionClass, constructor parameters]
     *
     * Проверяет на существование конструктора и передает значения по умолчанию
     * Возвращает массив с элементами [\ReflectionClass, праметры конструктора]
     * @param $service
     * @param array $params
     * @return array
     * @throws ServiceException
     */
    private function dependencies($service, array &$params = []) :array
    {
        $di = $params['di'] ?? null;
        unset($params['di']);

        $dependencies = [];
        $reflection = $this->ReflectionClass($service);
        $constructor = $reflection->getConstructor();
        if ($constructor !== null){
            foreach ($constructor->getParameters() as $parameter) {
                if ($parameter->isDefaultValueAvailable()){
                    $dependencies[] = $parameter->getDefaultValue();
                }else {
                    /* If constructor parameters are not set by default   */
                    /* Если параметры конструктора не заданы по умолчанию */
                    if ($parameter->getType() === null) {
                        $className = $parameter->getDeclaringClass()->getName();
                        $paramName = $parameter->getName();
                        throw new ServiceException(sprintf('Missing required parameter "%s" when instantiating "%s".',$paramName, $className));
                    }
                    $class = $parameter->getClass();
                    if ($class->isInterface()){
                        if (null === $di){
                            throw new ServiceException(sprintf('%s::__construct() must implement interface %s',$reflection->getName(), $class->getName()));
                        }
                        /* @var \ReflectionClass $di */
                        if (!$di->implementsInterface($class->getName())) {
                            throw new ServiceException(sprintf('%s must implement interface %s', $di->getName(), $class->getName()));
                        }
                        $class = $di;
                    }
                    $dependencies[] = $class === null ? null : $class->getName();
                }
            }
        }
        return [$reflection, $dependencies];
    }
}